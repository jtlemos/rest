﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using rest.Models;

namespace rest.Controllers
{
    public class AutorController : ApiController
    {
        public IEnumerable<Autor> Get()
        {
            return SimulacaoAutor.ObterTodosAutores();
        }
    }
}
